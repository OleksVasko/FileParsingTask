package com.fileparser.test;

import com.homework.Launcher;
import com.homework.ParallelStreamFileProcessor;
import com.homework.utils.ColumnConfigXMLParser;
import com.homework.utils.RowConfigXMLParser;
import com.homework.utils.TsvReader;
import com.homework.utils.TsvWriter;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AllTests {

    private static List<Map<String, Object>> data;
    private static ParallelStreamFileProcessor processor;
    private static Properties properties = new Properties();

    @BeforeAll
    static void init() throws IOException {
        data = new ArrayList<>();
        Map<String, Object> map1 = new HashMap<>();
        map1.put("ID", "ID_1");
        map1.put("COL_1", "12345");
        map1.put("COL_2", "12345");
        map1.put("COL_3", "12345");
        data.add(map1);
        Map<String, Object> map2 = new HashMap<>();
        map2.put("ID", "ID_2");
        map2.put("COL_1", "12345");
        map2.put("COL_2", "12345");
        map2.put("COL_3", "12345");
        data.add(map2);
        Map<String, Object> map3 = new HashMap<>();
        map3.put("ID", "ID_3");
        map3.put("COL_1", "12345");
        map3.put("COL_2", "12345");
        map3.put("COL_3", "12345");
        data.add(map3);

        try (InputStream input = new FileInputStream("src/test/resources/configuration.properties")) {
            properties.load(input);
        }
        processor = new ParallelStreamFileProcessor(properties);

    }

    @Test
    void filterTest() {
        List<Map<String, Object>> testData = new ArrayList<>();
        Map<String, Object> map1 = new HashMap<>();
        map1.put("ID", "OUR_ID_1");
        map1.put("OUR_COL_1", "12345");
        map1.put("OUR_COL_2", "12345");
        testData.add(map1);
        Map<String, Object> map2 = new HashMap<>();
        map2.put("ID", "OUR_ID_2");
        map2.put("OUR_COL_1", "12345");
        map2.put("OUR_COL_2", "12345");
        testData.add(map2);
        assertEquals(testData, processor.filter(data));

    }

    @Test
    void columnParserTest() {
        ColumnConfigXMLParser parser = new ColumnConfigXMLParser(properties.getProperty("column.mapping.config"));
        Map<String, String> baselineMap = new HashMap<>();
        baselineMap.put("COL_1", "OUR_COL_1");
        baselineMap.put("COL_2", "OUR_COL_2");
        baselineMap.put("ID", "ID");
        assertEquals(baselineMap, parser.parse());
    }

    @Test
    void rowParserTest() {
        RowConfigXMLParser parser = new RowConfigXMLParser(properties.getProperty("row.mapping.config"));
        Map<String, String> baselineMap = new TreeMap<>();
        baselineMap.put("ID_1", "OUR_ID_1");
        baselineMap.put("ID_2", "OUR_ID_2");
        assertEquals(baselineMap, parser.parse());

    }

    @Test
    void listInputFileTest() throws IOException {
        String inputDir = properties.getProperty("data.input.dir");
        List<String> files = new ArrayList<>();
        files.add(Paths.get(inputDir + "InputData1.txt").toAbsolutePath().toString());
        assertEquals(files, Launcher.listInputFiles(inputDir));
    }

    @Test
    void dataFileReader() throws IOException {
        String filePath = Launcher.listInputFiles(properties.getProperty("data.input.dir")).get(0);
        TsvReader reader = new TsvReader(Integer.parseInt(properties.getProperty("input.data.column.number")));
        assertEquals(data, reader.read(filePath));

    }

    @Test
    void dataFileWriter() {
        TsvReader reader = new TsvReader(Integer.parseInt(properties.getProperty("input.data.column.number")));
        TsvWriter writer = new TsvWriter();
        writer.write(data);
        assertEquals(data, reader.read(properties.getProperty("data.output.dir") + "/outputFile-1.txt"));
    }

    @AfterAll
    static void cleaningHouse() throws IOException {
        System.out.println("Finishing test execution");
        Files.deleteIfExists(Paths.get(properties.getProperty("data.output.dir") + "/outputFile-1.txt"));
    }
}
