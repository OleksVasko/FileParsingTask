package com.homework;

import com.homework.utils.TsvReader;
import com.homework.utils.TsvWriter;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;


public class Launcher {

    private static Properties properties;
    private final static Logger LOG = Logger.getLogger(Launcher.class);

    public static void main(String[] args) throws Exception {
        LOG.info("Starting the program execution");
        properties = new Properties();
        try (InputStream input = new FileInputStream("src/main/resources/configuration.properties")) {
            properties.load(input);
        }

        doTheJob();
        LOG.info("Execution finished. Shutting down.");
    }

    /**
     * Method that reads the list of files in hte target directory.
     *
     * @param directory - string representation of directory path.
     * @return the list of all files in a specified directory
     * @throws IOException in case directory was not found.
     */

    public static List<String> listInputFiles(String directory) throws IOException {
        LOG.info("Scanning the input directory");
        DirectoryStream<Path> paths = Files.newDirectoryStream(Paths.get(directory));
        List<String> fileList = new ArrayList<>();
        for (Path p : paths) {
            LOG.info(p.toAbsolutePath().toString());
            fileList.add(p.toAbsolutePath().toString());
        }
        return fileList;
    }

    /**
     * Method that triggers all processing execution including reading data from files,
     * filtering according to column names and row ids specified in XML configuration files provided,
     * and writing filtered results into destination files.
     *
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     */

    private static void doTheJob() throws IOException, ParserConfigurationException, SAXException {
        TsvReader reader = new TsvReader(Integer.parseInt(properties.getProperty("input.data.column.number")));
        TsvWriter writer = new TsvWriter();
        ParallelStreamFileProcessor processor = new ParallelStreamFileProcessor(properties);

        listInputFiles(properties.getProperty("data.input.dir"))
                .parallelStream()
                .map(reader::read)
                .map(processor::filter)
                .forEach(writer::write);
    }


}
