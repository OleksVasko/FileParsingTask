package com.homework;

import com.homework.utils.ColumnConfigXMLParser;
import com.homework.utils.Parser;
import com.homework.utils.RowConfigXMLParser;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Class that does data filtering depending on the names of rows and columns specified in
 * row mapping and column mapping configs. Path to the config is specified by the properties file.
 */
public class ParallelStreamFileProcessor {
    private static Map<String, String> rowMapping;
    private static Map<String, String> columnMapping;



    public ParallelStreamFileProcessor(Properties properties){
        Parser<Map<String, String>> rowParser = new RowConfigXMLParser(properties.getProperty("row.mapping.config"));
        Parser<Map<String, String>> columnParser = new ColumnConfigXMLParser(properties.getProperty("column.mapping.config"));
        rowMapping = rowParser.parse();
        columnMapping = columnParser.parse();
    }

    /**
     * Filters the data retaining only those columns and rows specified by rowMapping and columnMapping configuration.
     * @param maps - data to be filtered
     * @return - returns filtered data in form of {@code java.util.List<Map<String, Object>>}
     */
    public List<Map<String, Object>> filter(List<Map<String, Object>> maps) {

        return maps.parallelStream()
//               filters out all rows that are not in a row mapping config file specified in configuration.properties file
                .filter(m -> rowMapping.keySet().contains(m.get("ID")))
//                retains only those columns specified in the column mapping config.
                .map(s -> {
                    Map<String, Object> map = new TreeMap<>(s);
                    map.keySet().retainAll(columnMapping.keySet());
                    return map;
                })
//                changes their column names to our names as per config
                .map(m -> m.entrySet().stream()
                        .collect(Collectors.toMap(e -> columnMapping.get(e.getKey()), Map.Entry::getValue))
                )
//                replaces their column id with our's as per row mapping config
                .peek(m->m.put("ID",rowMapping.get(m.remove("ID")))
                )
//                converts HashMap into TreeMap, that way our elements will be sorted the same way by column name, and
//                there should be no situation when two different rows will keep entries in a different order as our columns
//                are sorted by name via sorting map element by key.
                .map(TreeMap::new)
                .collect(Collectors.toList());
    }
}
