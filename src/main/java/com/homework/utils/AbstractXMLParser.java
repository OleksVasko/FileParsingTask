package com.homework.utils;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Map;

public abstract class AbstractXMLParser implements Parser<Map<String,String>> {

    private Document doc;
    private final static Logger LOG = Logger.getLogger(AbstractXMLParser.class);
    public abstract Map<String,String> parse();

    public AbstractXMLParser(String fileName)  {
        LOG.info("Starting to parse XML file "+ fileName);
        try {
        File fXmlFile = new File(fileName);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        doc = dBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();
        }
        catch (ParserConfigurationException | IOException | SAXException e) {
            LOG.error("Error parsing XML file "+fileName+". Please make sure that file exists and it has a correct structure. \n", e);
        }
    }

    public Document getDocument() {
        return doc;
    }
}
