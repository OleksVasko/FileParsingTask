package com.homework.utils;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.HashMap;
import java.util.Map;


/**
 * Class for reading simple XML configuration file with a column mapping to store result into {@code java.util.HashMap}.
 *
 * TO DO: in case of multiple configuration files should be received from XML parser factory get method depending on the
 * config tab. Also should be generified together with {@code com.homework.utils.RowConfigXMLParser}
 */
public class ColumnConfigXMLParser extends AbstractXMLParser {

    public ColumnConfigXMLParser(String fileName) {
        super(fileName);
    }
    private final static Logger LOG = Logger.getLogger(RowConfigXMLParser.class);
    @Override
    public Map<String, String> parse() {
        LOG.info("Starting to parse column configuration xml");
        Map<String, String> mappingMap = new HashMap<>();
        NodeList nodeList = getDocument().getElementsByTagName("mapping");
        for (int temp = 0; temp < nodeList.getLength(); temp++) {
            Node nNode = nodeList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element e = (Element) nNode;
                mappingMap.put(e.getElementsByTagName("sourceCol").item(0).getTextContent(),
                        e.getElementsByTagName("destinationCol").item(0).getTextContent());
            }
        }
        return mappingMap;
    }
}
