package com.homework.utils;


/**
 * This enum specifies CellProcessor[] type that can be returned by {@code com.homework.utils.CellProcessorFactory}
 * Please update this enum with new processor type entries if you add a new CellProcessor creation method to the
 * {@code com.homework.utils.CellProcessorFactory}.
 */
public enum ProcessorType {

    SIMPLE_PROCESSOR, COMPLEX_PROCESSOR
}
