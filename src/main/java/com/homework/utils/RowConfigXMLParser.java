package com.homework.utils;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


/**
 * Class for reading simple XML configuration file with a row mapping to store result into {@code java.util.HashMap}.
 * <p>
 * TO DO: in case of multiple configuration files should be received from XML parser factory get method depending on the
 * config tab. Also should be generified together with {@code com.homework.utils.ColumnConfigXMLParser}
 */
public class RowConfigXMLParser extends AbstractXMLParser {

    private final static Logger LOG = Logger.getLogger(RowConfigXMLParser.class);

    public RowConfigXMLParser(String fileName) {
        super(fileName);
    }

    public Map<String, String> parse() {
        LOG.info("Starting to parse row configuration xml");
        Map<String, String> mappingMap = new HashMap<>();
        NodeList nodeList = getDocument().getElementsByTagName("mapping");
        for (int temp = 0; temp < nodeList.getLength(); temp++) {
            Node nNode = nodeList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element e = (Element) nNode;
                mappingMap.put(e.getElementsByTagName("sourceId").item(0).getTextContent(),
                        e.getElementsByTagName("destinationId").item(0).getTextContent());
            }
        }
        return new TreeMap<>(mappingMap);
    }
}
