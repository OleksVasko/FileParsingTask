package com.homework.utils;

import org.apache.log4j.Logger;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.prefs.CsvPreference;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.homework.utils.ProcessorType.SIMPLE_PROCESSOR;

/**
 * Class for reading tsv files. It uses the super-csv framework.
 * The framework allows to read data from CSV or TAB-delimited files into:
 * - custom object if the column names match the fields of the domain object;
 * - {@code java.util.List} of {@code java.util.HashMap} where the key of the map is the
 * column name and the value is the cell value;
 * - or {@code java.util.ArrayList} of elements;
 * For more information please visit https://github.com/super-csv/super-csv
 */
public class TsvReader {
    private int defaultColumnNumber;

    public TsvReader(int colNumber){
      this.defaultColumnNumber = colNumber;
    }

    private final static Logger LOG = Logger.getLogger(TsvReader.class);

    /**
     * The method reads CSV or TAB-delimited file into {@code java.util.List} of {@code java.util.HashMap} objects.
     * Type of the file to read and other parameters are specified by {@code CsvPreference} enum.
     * @param fileName - the name of the file to read.
     * @return - returns {@code java.util.List<Map<String,Object>>} where elements of the list are maps
     * with column name as a key and cell data as a value. Data can be any of primitive types or {@code java.lang.String}
     *
     */
    public List<Map<String,Object>> read(String fileName)  {
        LOG.info("Reading the file "+fileName);
        List<Map<String, Object>> list = new ArrayList<>();
        try (ICsvMapReader mapReader = new CsvMapReader(new FileReader(fileName), CsvPreference.TAB_PREFERENCE)) {
            // the header columns are used as the keys to the Map
            final String[] header = mapReader.getHeader(true);
            final CellProcessor[] processors = CellProcessorFactory.get(SIMPLE_PROCESSOR, defaultColumnNumber);
            Map<String, Object> map;
            while ((map = mapReader.read(header, processors)) != null) {
                list.add(map);
            }
        } catch (Exception e){
            LOG.error("Error processing the file "+fileName+
                    ". Please make sure that file exists and available for reading.\n",e);
            e.printStackTrace();
        }
        return list;
    }
}
