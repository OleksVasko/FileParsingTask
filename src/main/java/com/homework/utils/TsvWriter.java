package com.homework.utils;

import org.apache.log4j.Logger;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import static com.homework.utils.ProcessorType.SIMPLE_PROCESSOR;


/**
 * Class for writing tsv files. It uses the super-csv framework.
 * The framework allows to write data to CSV or TAB-delimited files from:
 * - custom object if the column names match the fields of the domain object;
 * - {@code java.util.List} of {@code java.util.HashMap} where the key of the map is the
 * column name and the value is the cell value;
 * - or {@code java.util.ArrayList} of elements;
 * For more information please visit https://github.com/super-csv/super-csv
 */
public class TsvWriter {

    private static AtomicInteger fileCounter = new AtomicInteger(0);
    private final static Logger LOG = Logger.getLogger(TsvWriter.class);
    private static final String PROPERTIES_FILE = "src/main/resources/configuration.properties";
    private Properties properties;

    /**
     * Method writes elements from the {@code java.util.List<Map<String, Object>> } passed as a parameter into
     * CSV or TAB-delimited text file. Format depends on the passed enum {@code CsvPreference}
     *
     * @param mapList - list of mapped data to be persisted into the file where element of the list represented by map
     *                corresponds to row and map key - column name and map value -cell data.
     */
    public void write(List<Map<String, Object>> mapList) {
        properties = new Properties();
        try (InputStream input = new FileInputStream(PROPERTIES_FILE)) {
            properties.load(input);
        } catch (IOException e) {
            LOG.error("Unable to open properties file " + Paths.get(PROPERTIES_FILE).toAbsolutePath());
        }
        String destinationDir = properties.getProperty("data.output.dir");
        String destinationFile = destinationDir + "outputFile-" + fileCounter.addAndGet(1) + ".txt";
        LOG.info("Writing output files to directory " + destinationDir);
        try (ICsvMapWriter mapWriter = new CsvMapWriter(new FileWriter(destinationFile),
                CsvPreference.TAB_PREFERENCE)) {
            if (mapList.size() > 0) {
                String[] header = mapList.get(0).keySet().toArray(new String[mapList.get(0).keySet().size()]);
                mapWriter.writeHeader(header);
                final CellProcessor[] processors = CellProcessorFactory.get(SIMPLE_PROCESSOR, header.length);
                for (Map<String, Object> map : mapList) {
                    mapWriter.write(map, header, processors);
                }
            }
        } catch (IOException e) {
            LOG.error("Unable to open file " + destinationFile +
                    " for writing. Please make sure that path exists and available for writing", e);
        }
    }
}
