package com.homework.utils;

import org.supercsv.cellprocessor.FmtBool;
import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.LMinMax;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.constraint.UniqueHashCode;
import org.supercsv.cellprocessor.ift.CellProcessor;

/**
 * CellProcessorFactory class that creates and holds different CellProcessor[] configurations for
 * different types of CSV files. Should be updated with new types of pocessors if needed.
 */

public class CellProcessorFactory {

    /**
     * Static factory method that returns CellProcessor[] specified by parameters.
     *
     * @param type - specify the required processor type from those enumerated in
     *             {@code com.homework.utils.ProcessorType}
     * @param size specifies the size of CellProcessor[] array. Required to create
     *             simple cell processor array which works for parsing of the
     *             CSV / TAB-delimited files where all columns are of the same type.
     *             May be 0 otherwise.
     * @return - returns an array of columns configuration where  each array element corresponds to
     * the type of the data in the file column.
     */
    public static CellProcessor[] get(ProcessorType type, int size) {

        switch (type) {
            case SIMPLE_PROCESSOR:
                return createSimpleProcessor(size);
            case COMPLEX_PROCESSOR:
                return createComplexProcessor();
        }
        throw new IllegalArgumentException("No valid processor type was specified");
    }

    /**
     * Returns more complex CellProcessor[] able to parse different types of data entries.
     * It is used here just for reference.
     *
     * @return - returns an array of columns configuration where  each array element corresponds to
     * the type of the data in the file column.
     */

    private static CellProcessor[] createComplexProcessor() {

        return new CellProcessor[]{
                new UniqueHashCode(),
                new NotNull(),
                new NotNull(),
                new FmtDate("dd/MM/yyyy"),
                new NotNull(),
                new Optional(new FmtBool("Y", "N")),
                new Optional(),
                new NotNull(),
                new NotNull(),
                new LMinMax(0L, LMinMax.MAX_LONG)
        };

    }

    private static CellProcessor[] createSimpleProcessor(int size) {
        final CellProcessor[] processors = new CellProcessor[size];
        for (int i = 0; i < processors.length; i++)
            processors[i] = new NotNull();
        return processors;
    }
}
