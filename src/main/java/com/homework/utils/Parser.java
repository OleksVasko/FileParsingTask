package com.homework.utils;

@FunctionalInterface
public interface Parser<R> {
    R parse();
}
